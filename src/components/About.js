import React from "react";

const About = () => (
    <div id="about">
        <div>
            <h2>About</h2>
            <p>This has been developed by H Ali for a coding test for Bouvet.</p>
            <p>Email: ali.dev.se@gmail.com</p>
        </div>
    </div>

);

export default About;