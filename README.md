
## Image Loader - Bouvet

This is a demo application developed in React ( frontend ) and PHP ( backend ).

[DEMO](http://imageloader.softhem.se/)

### `npm install`
Runs the installation process.



### `npm start`
In the project directory, you can run the above command.
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `backend server`
This is optional as the  default one is a live server and will work with frontend. <br>
This is needed to solve CORS problem else you can change server address in api/feed.js.<br>
Inside backend directory run: <br>
php -S localhost:8888

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
